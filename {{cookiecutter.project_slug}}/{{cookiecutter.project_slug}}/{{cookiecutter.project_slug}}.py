"""Main Module."""


def moin() -> str:
    """Send a hearty 'Moin!'.

    Returns:
        'Moin!'
    """
    return 'Moin!'
