"""Module tests for {{cookiecutter.project_slug}}.{{cookiecutter.project_slug}}."""
import os
import sys

from {{cookiecutter.project_slug}}.{{cookiecutter.project_slug}} import moin

sys.path.insert(0, os.path.dirname(__file__))


def test_moin() -> None:
    assert moin() == 'Moin!'
