# cookiecutter-python



## Getting started

```bash
cookiecutter https://gitlab.com/MrCollaborator/cookiecutter-python
```

## Features

- Import sorting with [isort](https://pycqa.github.io/isort/)
- Linting with [Flake8](https://flake8.pycqa.org/en/latest/) Plugin [wemake-python-styleguide](https://wemake-python-styleguide.readthedocs.io/en/latest/)
- Static Type Checking with [mypy](https://mypy-lang.org/)
